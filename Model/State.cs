﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testing_WinApp.Model
{
    public class State: INotifyPropertyChanged
    {
        
        string usState;
        public string USState {
            get {return usState; }
            set
            {
                usState = value;
                OnPropertyChanged("USState"); }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
