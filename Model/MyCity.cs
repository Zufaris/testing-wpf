﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testing_WinApp.Model
{
    public class MyCity
    {
        public string Town { get; set; }
        public int Population { get; set; }


        public override string ToString()
        {
            return $"{Town} ({Population}) selected";
        }
    }

    
}
