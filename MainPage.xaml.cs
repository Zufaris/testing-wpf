﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using testing_WinApp.Model;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x419

namespace testing_WinApp
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        public string name = "Alex";
        public State Data = new State();
        ObservableCollection<MyCity> data = new ObservableCollection<MyCity>();
        //data.SelectionMode = SelectionMode.MultiSingle;

        public MainPage()
        {
            
            this.InitializeComponent();
            Data.USState = "Alabma";
            InitStates();
        }

        private void AssociateName_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            MessageDialog message = new MessageDialog("Clicked " + sender.ToString());
            message.ShowAsync().AsTask();
        }

       

        void InitStates()
        {
            data.Add(new MyCity()    {
                Town = "NY",
                Population = 90

            });
            data.Add(new MyCity()
            {
                Town = "Los Angeles",
                Population = 80

            }); ;
            data.Add(new MyCity()
            {
                Town = "Washington",
                Population = 60

            });
        }

        private void AddCity_Click(object sender, RoutedEventArgs e)
        {
            data.Add(new MyCity()
            {
                Town = "Samara",
                Population = 34
            });
        }

        private void RemoveCity_Click(object sender, RoutedEventArgs e)
        {
            data.Remove(State.SelectedItem as MyCity);
        }
    }
}
